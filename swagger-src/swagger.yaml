swagger: '2.0'

info:
  title: Imaginary airport
  description: Airport dispatcher API
  version: 0.0.42
consumes:
  - application/json
produces:
  - application/json

basePath: /api/v1

responses:
  '201':
    description: Ресурс создан
    headers:
      Location:
        description: Url созданного ресурса
        type: string
  '204':
    description: Ресурс удален
  '400':
    description: Ошибка валидации входных данных
  '404':
    description: Ресурс не найден
  '405':
    description: Самолету пора на техобслуживание
  '409':
    description: Этот ресурс используется в других объектах

tags:
- name: planeType
  description: Типы самолетов
- name: plane
  description: Самолеты
- name: crew
  description: Экипажи, пилоты, стюардессы
- name: flights
  description: Рейсы
- name: tickets
  description: Билеты
- name: departures
  description: Вылеты

parameters:
  id:
    name: id
    in: path
    type: integer
    format: int64
    required: true
    minimum: 1

paths:
  /planeTypes:
    get:
      tags: [planeType]
      summary: Получить список всех типов самолетов
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/PlaneType'
    post:
      tags: [planeType]
      summary: Создать новый тип самолета
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/PlaneType'
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
  '/planeTypes/{id}':
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [planeType]
      summary: Получить тип самолета
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/PlaneType'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    put:
      tags: [planeType]
      summary: Изменить тип самолета
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/PlaneType'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/PlaneType'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [planeType]
      summary: Удалить тип самолета
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
  /planes:
    get:
      tags: [plane]
      summary: Получить список самолетов
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Plane'
    post:
      tags: [plane]
      summary: Поставить самолет на учет
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Plane'
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
  '/planes/{id}':
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [plane]
      summary: Получить информацию о самолете
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Plane'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    put:
      tags: [plane]
      summary: Изменить информацию о самолете
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Plane'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Plane'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [plane]
      summary: Удалить самолет
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
  /pilots:
    get:
      tags: [crew]
      summary: Получить список пилотов
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Pilot'
    post:
      tags: [crew]
      summary: Принять пилота на работу
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Pilot'
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
  '/pilots/{id}':
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [crew]
      summary: Получить информацию о пилоте
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Pilot'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    put:
      tags: [crew]
      summary: Изменить информацию о пилоте
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Pilot'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Pilot'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [crew]
      summary: Уволить пилота
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
  /stewardesses:
    get:
      tags: [crew]
      summary: Получить список стюардесс
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Stewardess'
    post:
      tags: [crew]
      summary: Принять стюардессу на работу
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Stewardess'
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
  '/stewardesses/{id}':
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [crew]
      summary: Получить информацию о стюардессе
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Stewardess'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    put:
      tags: [crew]
      summary: Изменить информацию о стюардессе
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Stewardess'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Stewardess'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [crew]
      summary: Уволить стюардессу
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
  /crews:
    get:
      tags: [crew]
      summary: Получить список экипажей
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Crew'
    post:
      tags: [crew]
      summary: Создать новый экипаж
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Crew'
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
  '/crews/{id}':
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [crew]
      summary: Получить состав экипажа
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Crew'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
  '/crews/{crewId}/pilot':
    parameters:
      - name: crewId
        in: path
        required: true
        type: integer
        format: int64
        minimum: 1
    get:
      tags: [crew]
      summary: Получить информацию о пилоте экипажа
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Pilot'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
  '/crews/{crewId}/pilot/{id}':
    parameters:
      - name: crewId
        in: path
        required: true
        type: integer
        format: int64
        minimum: 1
      - $ref: '#/parameters/id'
    put:
      tags: [crew]
      summary: Назначить пилота в экипаж
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Pilot'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
    delete:
      tags: [crew]
      summary: Исключить пилота из экипажа
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
  '/crews/{crewId}/stewardess':
    parameters:
      - name: crewId
        in: path
        required: true
        type: integer
        format: int64
        minimum: 1
    get:
      tags: [crew]
      summary: Получить список стюардесс экипажа
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Stewardess'
  '/crews/{crewId}/stewardess/{id}':
    parameters:
      - name: crewId
        in: path
        required: true
        type: integer
        format: int64
        minimum: 1
      - $ref: '#/parameters/id'
    post:
      tags: [crew]
      summary: Добавить стюардессу в экипаж
      responses:
        '201':
          $ref: '#/responses/201'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
    delete:
      tags: [crew]
      summary: Исключить стюардессу из экипажа
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
  /flights:
    get:
      tags: [flights]
      summary: Получить список рейсов
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Flight'
    post:
      tags: [flights]
      summary: Создать рейс
      description: >
        Поскольку у нас нет никакой возможности создавать билеты,  то передается
        дополнительный массив объектов {.count, .price}, из  которого будут
        сформированы доступные билеты рейса.
      parameters:
        - name: flightSched
          in: body
          required: true
          schema:
            type: object
            properties:
              flight:
                $ref: '#/definitions/Flight'
              ticketParams:
                type: array
                items:
                  type: object
                  properties:
                    count:
                      type: integer
                      minimum: 1
                    price:
                      type: number
                      format: decimal
                      multipleOf: 0.01
                  required:
                  - count
                  - price
                minLength: 1
            required:
            - flightId
            - ticketParams
      responses:
        '201':
          description: OK
          schema:
            $ref: '#/definitions/Flight'
        '400':
          $ref: '#/responses/400'
        '405':
          $ref: '#/responses/405'
  /flights/{id}:
    parameters:
      - $ref: '#/parameters/id'
    get:
      tags: [flights]
      summary: Получить информацию о рейсе
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Flight'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    put:
      tags: [flights]
      summary: Изменить информацию о рейсе
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Flight'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Flight'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [flights]
      summary: Отменить рейс
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
  /flights/{id}/tickets:
    parameters:
    - $ref: '#/parameters/id'
    post:
      tags: [tickets]
      summary: Купить билет
      parameters:
        - name: ticketType
          in: body
          required: true
          schema:
            $ref: '#/definitions/TicketPref'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Ticket'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '410':
          description: Билетов больше нет
    delete:
      parameters: 
      - name: ticket
        in: body
        required: true
        schema:
          $ref: '#/definitions/Ticket'
      tags: [tickets]
      summary: Вернуть билет
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
  /departures:
    get:
      tags: [departures]
      summary: Получить список вылетов
      responses:
        '200':
          description: OK
          schema:
            type: array
            items:
              $ref: '#/definitions/Departure'
    post:
      tags: [departures]
      summary: Запланировать вылет
      responses:
        '201':
          description: OK
          schema:
            $ref: '#/definitions/Departure'
        '400':
          $ref: '#/responses/400'
        '405':
          $ref: '#/responses/405'
        '409':
          $ref: '#/responses/409'
  /departures/{id}:
    parameters:
    - $ref: '#/parameters/id'
    get: 
      tags: [departures]
      summary: Получить информацию о вылете
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Departure'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    post:
      tags: [departures]
      summary: Совершить вылет
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Departure'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Departure'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '405':
          $ref: '#/responses/405'
        '409':
          $ref: '#/responses/409'
    put:
      tags: [departures]
      summary: Изменить информацию о вылете
      parameters:
        - name: body
          in: body
          required: true
          schema:
            $ref: '#/definitions/Departure'
      responses:
        '200':
          description: OK
          schema:
            $ref: '#/definitions/Departure'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
    delete:
      tags: [departures]
      summary: Отменить вылет
      responses:
        '204':
          $ref: '#/responses/204'
        '400':
          $ref: '#/responses/400'
        '404':
          $ref: '#/responses/404'
        '409':
          $ref: '#/responses/409'
    
definitions:
  Ticket:
    description: Билет
    type: object
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      price:
        type: number
      flightId:
        type: string
    required:
      - price
      - flightId
  TicketPref:
    description: Предпочтительный тип билета
    type: object
    properties:
      kind:
        type: string
        enum:
        - minPrice
        - maxPrice
        - any
    required:
    - kind
  PlaneType:
    description: Тип самолета
    type: object
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      model:
        type: string
      seats:
        type: integer
      capacity:
        type: number
    required:
      - model
      - seats
      - capacity
  Plane:
    description: Самолет
    type: object
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      planeTypeId:
        type: integer
        format: int64
        minimum: 1
      name:
        type: string
      createdDate:
        type: string
        format: date
      lifetime:
        type: integer
        format: int64
        minimum: 0
    required:
      - planeTypeId
      - name
      - createdDate
      - lifetime
  Pilot:
    description: Пилот
    type: object
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      firstName:
        type: string
      lastName:
        type: string
      birthDate:
        type: string
        format: date
      experience:
        type: string
    required:
      - firstName
      - lastName
      - birthDate
      - experience
  Stewardess:
    description: Стюардесса
    type: object
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      firstName:
        type: string
      lastName:
        type: string
      birthDate:
        type: string
        format: date
    required:
      - firstName
      - lastName
      - birthDate
  Crew:
    description: Экипаж
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      pilotId:
        type: integer
        format: int64
        minimum: 1
      stewardesses:
        type: array
        items:
          type: integer
          format: int64
          minimum: 1
  Flight:
    description: Рейс
    properties:
      id:
        type: string
      source:
        type: string
      departureDate:
        type: string
        format: date-time
      destination:
        type: string
      arrivalDate:
        type: string
        format: date-time
      tickets:
        type: array
        items:
          type: integer
          format: int64
          minimum: 1
    required:
      - source
      - departureDate
      - destination
      - arrivalDate
  Departure:
    description: Вылет
    properties:
      id:
        type: integer
        format: int64
        minimum: 1
      flightId:
        type: integer
        format: int64
        minimum: 1
      departureDate:
        type: string
        format: date-time
      crewId:
        type: integer
        format: int64
        minimum: 1
      planeId:
        type: integer
        format: int64
        minimum: 1
    required:
      - flightId
      - crewId
      - planeId
