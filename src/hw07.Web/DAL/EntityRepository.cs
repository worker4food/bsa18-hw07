using System;
using System.Linq;
using System.Collections.Generic;
using hw07.Web.Models;

namespace hw07.DAL
{
    public class EntityRepository<TKey, T> : IRepository<TKey, T>
        where T : Entity<TKey>
    {
        IDataContext _context;
        IDictionary<TKey, T> Items { get; }

        public EntityRepository(IDataContext c)
        {
            _context = c;
            Items = c.Set<T>()
                .ToDictionary(x => x.Id);
        }

        public IEnumerable<T> GetAll() =>
            Items.Values;
        
        public T Get(TKey id)
        {
            if(Items.TryGetValue(id, out var item))
                return item;
            else
                return null;
        }
        
        public void Insert(T item) =>
            Items.Add(item.Id, item);

        public void Delete(TKey id) =>
            Items.Remove(id);

        public void Update(T item)
        {
            Items[item.Id] = item;
        } 

        public void Save() {}

        public TKey LastId() =>
            Items.Keys.Max();
    }
}