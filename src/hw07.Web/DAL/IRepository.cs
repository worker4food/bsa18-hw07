using System;
using System.Collections.Generic;

namespace hw07.DAL
{
    public interface IRepository<TKey, T>
    {
        IEnumerable<T> GetAll();
        T Get(TKey id);
        void Insert(T T);
        void Delete(TKey id);
        void Update(T item);
        void Save();

        TKey LastId();
    }
}