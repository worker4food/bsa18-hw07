using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using hw07.Models.Dto;
using hw07.Web.Models;

namespace hw07.DAL
{
    public class AirportDataContext : IDataContext
    {
        IEnumerable<PlaneType> PlaneType { get; set; }
        IEnumerable<Plane> Plane { get; set; }
        IEnumerable<Pilot> Pilot { get; set; }  
        IEnumerable<Stewardess> Stewardess { get; set; }  
        IEnumerable<Crew> Crew { get; set; }  
        IEnumerable<Flight> Flight { get; set; }
        IEnumerable<Departure> Departure { get; set; }   
        IEnumerable<Ticket> Ticket { get; set; }

        public IMapper DirectMapper { get; }

        public AirportDataContext()
        {
            var planeTypes = loadResource<PlaneTypeDto>("planetypes");
            var planes = loadResource<PlaneDto>("planes");
            var pilots = loadResource<PilotDto>("pilots");
            var stewardesses = loadResource<StewardessDto>("stewardesses");
            var crews = loadResource<CrewDto>("crews");
            var flights = loadResource<FlightDto>("flights");
            var tickets = loadResource<TicketDto>("tickets");
            var departures = loadResource<DepartureDto>("departures");

            DirectMapper = new MapperConfiguration(cfg => {
                    cfg.CreateMap<PlaneDto, Plane>()
                        .ForMember(
                            p => p.PlaneType, 
                            opt => opt.MapFrom(src => 
                                PlaneType.First(pt => pt.Id == src.PlaneTypeId)));

                    cfg.CreateMap<CrewDto, Crew>()
                        // .ForMember(dest => dest.Stewardesses,
                        //     opt => opt.MapFrom(src =>
                        //         Stewardess.Where(s => src.Stewardesses.Contains(s.Id))))
                        .ForMember(dest => dest.Pilot,
                            opt => opt.MapFrom(src => 
                                Pilot.First(p => p.Id == src.PilotId)));
                    
                    cfg.CreateMap<TicketDto, Ticket>()
                        .ForMember(dest => dest.Flight,
                            opt => opt.MapFrom(src =>
                                flights.First(f => f.Id == src.FlightId)));

                    cfg.CreateMap<DepartureDto, Departure>()
                        .ForMember(dest => dest.Crew,
                            opt => opt.MapFrom(src =>
                                Crew.First(c => c.Id == src.CrewId)))
                        .ForMember(dest => dest.Flight,
                            opt => opt.MapFrom(src =>
                                Flight.First(f => f.Id == src.FlightId)))
                        .ForMember(dest => dest.Plane,
                            opt => opt.MapFrom(src =>
                                Plane.First(p => p.Id == src.PlaneId)));
                })
                .CreateMapper();
    
            PlaneType = from pt in planeTypes
                select DirectMapper.Map<PlaneTypeDto, PlaneType>(pt);

            Plane = from p in planes
                select DirectMapper.Map<PlaneDto, Plane>(p);

            Pilot = from p in pilots
                select DirectMapper.Map<PilotDto, Pilot>(p);

            Stewardess = from s in stewardesses
                select DirectMapper.Map<StewardessDto, Stewardess>(s);

            Crew = from c in crews
                select DirectMapper.Map<CrewDto, Crew>(c);

            Flight = from f in flights
                select DirectMapper.Map<FlightDto, Flight>(f);

            Ticket = from t in tickets
                select DirectMapper.Map<TicketDto, Ticket>(t);
            
            Departure = from d in departures
                select DirectMapper.Map<DepartureDto, Departure>(d);
        }

        private IEnumerable<T> loadResource<T>(string name)
        {
            var fullName = $"hw07.Web.GeneratedData.{name}.json";

            Console.WriteLine($"Loading {fullName}");

            using(var s = typeof(AirportDataContext).Assembly.GetManifestResourceStream(fullName))
            using(var sr = new StreamReader(s))
            {
                return JsonConvert.DeserializeObject<IEnumerable<T>>(sr.ReadToEnd());
            }
        }

        public IEnumerable<T> Set<T>() 
        {
            return PlaneType as IEnumerable<T>
                ?? Plane as IEnumerable<T>
                ?? Pilot as IEnumerable<T>
                ?? Stewardess as IEnumerable<T>
                ?? Crew as IEnumerable<T>
                ?? Flight as IEnumerable<T>
                ?? Departure as IEnumerable<T>
                ?? Ticket as IEnumerable<T>
                ?? throw new InvalidOperationException($"No dataset for type {typeof(T).FullName}");
        }
    }
}