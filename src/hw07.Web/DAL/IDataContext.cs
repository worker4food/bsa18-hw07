using System.Collections.Generic;
using AutoMapper;

namespace hw07.DAL
{
    public interface IDataContext
    {
        IEnumerable<T> Set<T>();

        ///<summary>
        ///Dto -> Model mapper
        ///</summary>
        IMapper DirectMapper { get; }
    }
}