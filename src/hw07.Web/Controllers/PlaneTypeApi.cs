/*
 * Imaginary airport
 *
 * Airport dispatcher API
 *
 * OpenAPI spec version: 0.0.42
 *
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using hw07.Web.Attributes;
using hw07.Models.Dto;
using hw07.Web.Models;
using hw07.Services;
using hw07.DAL;

namespace hw07.Web.Controllers
{
    /// <summary>
    /// Plane type API
    /// </summary>
    public class PlaneTypeApiController : Controller
    {
        private AirportService service;
        private IMapper directMapper;
        public PlaneTypeApiController(AirportService svc, IDataContext ctx) 
        {
            service = svc;
            directMapper = ctx.DirectMapper;
        }

        /// <summary>
        /// Get list of plane models
        /// </summary>

        /// <response code="200">OK</response>
        [HttpGet]
        [Route("/api/v1/planeTypes")]
        [ValidateModelState]
        [SwaggerOperation("PlaneTypesGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(IEnumerable<PlaneTypeDto>), description: "OK")]
        public virtual IActionResult PlaneTypesGet()
        {
            var res = service.PlaneType.GetAll()
                .Select(x => Mapper.Map<PlaneType, PlaneTypeDto>(x));

            return new ObjectResult(res);
        }

        /// <summary>
        /// Delete plane model
        /// </summary>

        /// <param name="id"></param>
        /// <response code="204">Ресурс удален</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        [HttpDelete]
        [Route("/api/v1/planeTypes/{id}")]
        [ValidateModelState]
        [SwaggerOperation("PlaneTypesIdDelete")]
        public virtual IActionResult PlaneTypesIdDelete([FromRoute][Required]long id)
        {
            var pt = service.PlaneType.Get(id);
            if(pt == null)
                return StatusCode(404);
            
            service.PlaneType.Delete(id);

            return StatusCode(204);
        }

        /// <summary>
        /// Get plane model info
        /// </summary>

        /// <param name="id"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        [HttpGet]
        [Route("/api/v1/planeTypes/{id}")]
        [ValidateModelState]
        [SwaggerOperation("PlaneTypesIdGet")]
        [SwaggerResponse(statusCode: 200, type: typeof(PlaneTypeDto), description: "OK")]
        public virtual IActionResult PlaneTypesIdGet([FromRoute][Required]long id)
        {
            // if(id == null)
            //     return StatusCode(400);

            var pt = service.PlaneType.Get(id);
            if(pt == null)
                return StatusCode(404);

            return new ObjectResult(Mapper.Map<PlaneTypeDto>(pt));
        }

        /// <summary>
        /// Update plane model
        /// </summary>

        /// <param name="id"></param>
        /// <param name="body"></param>
        /// <response code="200">OK</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        /// <response code="404">Ресурс не найден</response>
        [HttpPut]
        [Route("/api/v1/planeTypes/{id}")]
        [ValidateModelState]
        [SwaggerOperation("PlaneTypesIdPut")]
        [SwaggerResponse(statusCode: 200, type: typeof(PlaneTypeDto), description: "OK")]
        public virtual IActionResult PlaneTypesIdPut([FromRoute][Required]long id, [FromBody]PlaneTypeDto body)
        {
            var pt = service.PlaneType.Get(id);

            if(pt == null)
                return StatusCode(404);
            else {
                body.Id = id;
                var newVal = directMapper.Map<PlaneType>(body);

                service.PlaneType.Update(newVal);

                return new ObjectResult(body);
            }
        }

        /// <summary>
        /// Create new plane momdel
        /// </summary>

        /// <param name="body"></param>
        /// <response code="201">Ресурс создан</response>
        /// <response code="400">Ошибка валидации входных данных</response>
        [HttpPost]
        [Route("/api/v1/planeTypes")]
        [ValidateModelState]
        [SwaggerOperation("PlaneTypesPost")]
        public virtual IActionResult PlaneTypesPost([FromBody]PlaneTypeDto body)
        {
            body.Id = service.PlaneType.LastId() + 1;
            var newPt = directMapper.Map<PlaneType>(body);

            service.PlaneType.Insert(newPt);

            return new ObjectResult(body);
        }
    }
}
