#! "netcoreapp2.0"
#r "bin/Debug/netcoreapp2.0/hw07.Web.dll"
#r "bin/Debug/netcoreapp2.0/hw07.Shared.dll" 
#r "nuget: AutoBogus, *"
#r "nuget: Bogus, *"
#r "nuget: Newtonsoft.Json, *"

using System;
using System.Collections.Generic;
using System.Linq;
using AutoBogus;
using Bogus;
using Bogus.DataSets;
using Newtonsoft.Json;

using hw07.Models.Dto;
using hw07.Web.Models;

var idSeqs = new Dictionary<string, long>();

long  newId<T>() {
    long id;

    idSeqs.TryGetValue(typeof(T).Name, out id);

    idSeqs[typeof(T).Name] = id + 1;

    return id + 1;
}

var planeModels = new string[] {"Boeing 777", "Boeing 747", "Airbus a320", "Airbus a380"};

var locale = "en";
var num = 10;
var planeTypes = new Faker<PlaneTypeDto>(locale)
    .Rules((f, pt) => {
        pt.Id = newId<PlaneTypeDto>();
        pt.Model = f.PickRandom(planeModels); //f.Finance.Bic();
        pt.Seats = f.Random.Number(5, 50);
        pt.Capacity = f.Random.Number(20, 500);
    })
    .Generate(num);

var planes = new Faker<PlaneDto>(locale)
    .Rules((f, p) => {
        p.Id = newId<PlaneDto>();
        p.PlaneTypeId = f.PickRandom(planeTypes).Id;
        p.Name = f.Commerce.ProductName();
        p.CreatedDate = f.Date.Past(10);
        p.Lifetime =f.Random.Number(2, 15);
    }).Generate(num * 4);

var pilots = new Faker<PilotDto>(locale)
    .Rules((f, p) => {
        p.Id = newId<PilotDto>();
        p.FirstName = f.Person.FirstName;
        p.LastName = f.Person.LastName;
        p.BirthDate = f.Date.Past(50, DateTime.Now.AddYears(-25));
        p.Experience = f.Random.Number(3, 15).ToString();
    }).Generate(num * 2);

var stewardesses = new Faker<StewardessDto>(locale)
    .Rules((f, p) => {
        p.Id = newId<StewardessDto>();
        p.FirstName = f.Name.FirstName(Name.Gender.Female);
        p.LastName = f.Name.LastName(Name.Gender.Female);
        p.BirthDate = f.Date.Past(30, DateTime.Now.AddYears(-20));
    })
    .Generate(num * 4);

IEnumerable<TicketDto> tickets = new List<TicketDto>();

var flightG = new Faker<FlightDto>(locale)
    .Rules((f, p) => {
        p.Id = new Bogus.Randomizer().Replace("??-###"); //f.Finance.Bic();
        p.Source = f.Address.Country();
        p.DepartureDate = f.Date.Soon(2);
        p.Destination = f.Address.Country();
        p.ArrivalDate = p.DepartureDate.Value.AddHours(f.Random.Number(8, 24));
        p.Destination = f.Address.Country();

        var ts = new Faker<TicketDto>(locale)
            .Rules((_, t) => {
                t.Id = newId<TicketDto>();
                t.FlightId = p.Id;
                t.Price = decimal.Parse(f.Commerce.Price(100));
            }).Generate(10);
        
        tickets = tickets.Concat(ts);

        p.Tickets = ts;
    });

var busyP = new HashSet<PilotDto>();
var busyS = new HashSet<StewardessDto>();

var crewG = new Faker<CrewDto>(locale)
    .Rules((f, c) => {
        c.Id = newId<CrewDto>();

        var newP = f.PickRandom(pilots.Where(p => !busyP.Contains(p)));
        busyP.Add(newP);
  
        c.PilotId = newP.Id;
    
        var newS0 = f.PickRandom(stewardesses.Where(s => !busyS.Contains(s)));
        busyS.Add(newS0);

        var newS1 = f.PickRandom(stewardesses.Where(s => !busyS.Contains(s)));
        busyS.Add(newS1);
        
        c.Stewardesses = new StewardessDto[2] {newS0, newS1};
    });

var r = new Random();
var flights = flightG.Generate(num);
var crews = crewG.Generate(num);

var departures = flights.OrderBy(_ => r.Next())
    .Skip(3)
    .Zip(crews.OrderBy(_ => r.Next()), (f, c) => (f, c))
    .Zip(planes.OrderBy(_ => r.Next()), (fc, p) => (flight: fc.f, crew: fc.c, plane: p))
    .Select(fc => new DepartureDto {
        Id = newId<DepartureDto>(),
        CrewId = fc.crew.Id,
        FlightId = fc.flight.Id,
        PlaneId = fc.plane.Id,
        DepartureDate = new Faker(locale).Date.Soon(1)
    });

void writeJson<T>(string name, IEnumerable<T> os) {
  File.WriteAllText($"GeneratedData/{name}.json", JsonConvert.SerializeObject(os, Formatting.Indented));  
}

writeJson("planetypes", planeTypes);
writeJson("planes", planes);
writeJson("pilots", pilots);
writeJson("stewardesses", stewardesses);
writeJson("crews", crews);
writeJson("flights", flights);
writeJson("departures", departures);
writeJson("tickets", tickets);

