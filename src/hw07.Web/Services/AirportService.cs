using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using hw07.DAL;
using hw07.Web.Models;

namespace hw07.Services
{
    public class AirportService
    {
        public IRepository<long, PlaneType> PlaneType { get; set; }
        public IRepository<long, Plane> Plane { get; set; }
        public IRepository<long, Pilot> Pilot { get; set; }  
        public IRepository<long, Stewardess> Stewardess { get; set; }  
        public IRepository<long, Crew> Crew { get; set; }  
        public IRepository<string, Flight> Flight { get; set; }
        public IRepository<long, Departure> Departure { get; set; }   
        public IRepository<long, Ticket> Ticket { get; set; }

        public AirportService(
            IRepository<long, PlaneType> PlaneType,
            IRepository<long, Plane> Plane,
            IRepository<long, Pilot> Pilot,
            IRepository<long, Stewardess> Stewardess,
            IRepository<long, Crew> Crew,
            IRepository<string, Flight> Flight,
            IRepository<long, Departure> Departure,
            IRepository<long, Ticket> Ticket)
        {
            this.PlaneType = PlaneType;
            this.Plane = Plane;
            this.Pilot = Pilot;
            this.Stewardess = Stewardess;
            this.Crew = Crew;
            this.Flight = Flight;
            this.Departure = Departure;
            this.Ticket = Ticket;
        }
    }
}